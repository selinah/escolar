﻿using System;
using Entidades.ControlEscolarApp;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class UsuarioModal : Form
    {
        private UsuarioManejador _usuariomanejador;
        private Usuarios _usuario;
        //private bool _isEnaBinding = false;
        public UsuarioModal()
        {
            InitializeComponent();
            _usuariomanejador = new UsuarioManejador();
            _usuario = new Usuarios();
           // BindingUsuario();
        }

        public UsuarioModal(Usuarios usuario)
        {
            InitializeComponent();
            _usuariomanejador = new UsuarioManejador();
            _usuario = new Usuarios();
            _usuario = usuario;
             loadentity();
            //_isEnaBinding = true;
        }
        private void loadentity()
        {
            txt_nombre.Text = _usuario.Nombre;
            txt_apellido_paterno.Text = _usuario.ApellidoPaterno;
            txt_apellido_materno.Text = _usuario.ApellidoMaterno;
        }

        private void UsuarioModal_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //BindingUsuario();
            //if(ValidarUsuario())
            //{
            try
            {
                Guardar();
                this.Close();
            }
            catch (Exception eee)
            {
                MessageBox.Show("Hubo un error: "+eee);
            }
            //}


        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
         void Guardar()
        {
            _usuario.Nombre = txt_nombre.Text+"";
            _usuario.ApellidoPaterno = txt_apellido_paterno.Text+"";
            _usuario.ApellidoMaterno = txt_apellido_materno.Text+"" ;
            _usuariomanejador.Guardar(_usuario);
            
        }
        /*private bool ValidarUsuario()
        {
            var res = _usuariomanejador.EsUsuarioValido(_usuario);
            if(!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }*/
        private void BindingUsuario()
        {
                _usuario.Nombre = txt_nombre.Text;
                _usuario.ApellidoPaterno = txt_apellido_paterno.Text;
                _usuario.ApellidoMaterno = txt_apellido_materno.Text;
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            //BindingUsuario();
        }

        private void txt_apellido_paterno_TextChanged(object sender, EventArgs e)
        {
            //BindingUsuario();
        }

        private void txt_apellido_materno_TextChanged(object sender, EventArgs e)
        {
           // BindingUsuario();
        }


    }
}
