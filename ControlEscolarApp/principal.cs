﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ControlEscolarApp;


namespace ControlEscolarApp
{
    public partial class principal : Form
    {
        public principal()
        {
            InitializeComponent();
        }

        private void btn_catalogo_Click(object sender, EventArgs e)
        {
            frm_usuarios f = new frm_usuarios();
            f.Show();
        }

        private void Alumnos_Click(object sender, EventArgs e)
        {
            FormularioAlumno fa = new FormularioAlumno();
            fa.Show();
        }

        private void Btn_escuelas_Click(object sender, EventArgs e)
        {
            FormularioEscuela fe = new FormularioEscuela();
            fe.Show();
        }

        private void Btn_maestros_Click(object sender, EventArgs e)
        {
            FormularioMaestros fm = new FormularioMaestros();
            fm.Show();
        }

        private void btnrespaldo_Click(object sender, EventArgs e)
        {
            Respaldo r = new Respaldo();
            r.Show();
        }

        private void btnmaterias_Click(object sender, EventArgs e)
        {
            MateriasModal m = new MateriasModal();
            m.Show();
        }

        private void btngrupo_Click(object sender, EventArgs e)
        {
            GrupoModal g = new GrupoModal();
            g.Show();
        }
    }
}
