﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolarApp;
using Logica_Negocio.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class FormularioMaestros : Form
    {
        MaestroManejador clase_Logicanegocio;
        Maestros lista_Entidades;
        public FormularioMaestros()
        {
            InitializeComponent();
            clase_Logicanegocio = new MaestroManejador() ;
            lista_Entidades = new Maestros();
        }
        private void buscar_datos(string filtro)
        {
            visor.DataSource = clase_Logicanegocio.ObtenerLista("");
        }

        private void EditarDocumentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab =
            tab_documento;
        }

        private void EditarRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab =
            tab_insertar;
        }

        private void EliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            lista_Entidades.No_control = txt_no_control.Text + "";
            clase_Logicanegocio.Eliminar(lista_Entidades);
        }

        private void Visor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Visor_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            buscar_datos("");
        }

        private void TextBox11_TextChanged(object sender, EventArgs e)
        {
            buscar_datos(textBox11.Text);
        }

        private void Txt_guardar_Click(object sender, EventArgs e)
        {
            guardar();
            //limpiar();
            buscar_datos("");
        }

        private void guardar()
        {

            lista_Entidades.No_control = txt_no_control.Text + "";
            lista_Entidades.Nombre = txt_nombre.Text + "";
            lista_Entidades.Apellido_paterno = txt_apat.Text + "";
            lista_Entidades.Apellido_materno = txt_amat.Text + "";
            lista_Entidades.Fecha_nacimiento = txt_nacimiento.Text + "";
            lista_Entidades.Estado = cmb_estado.Text + "";
            lista_Entidades.Municipio = cmb_municipio.Text + "";
            lista_Entidades.Email = txt_cooreo.Text + "";
            lista_Entidades.Sexo = cmb_sexo.Text + "";
            lista_Entidades.No_tarjeta = txt_no_control.Text + "";
            lista_Entidades.Nombre_doc = lbl_nombre_doc.Text + "";
            clase_Logicanegocio.Guardar(lista_Entidades);
        }
        private void limpiar()
        {
            txt_no_control.Text = "";
            txt_nombre.Text = "";
            txt_apat.Text = "";
            txt_amat.Text = "";
            txt_nacimiento.Text = "";
            cmb_estado.Text = "";
            cmb_municipio.Text = "";
            txt_cooreo.Text = "";
            cmb_sexo.Text = "";
            txt_no_control.Text = "";
            lbl_nombre_doc.Text = "";
        }

        private void Txt_cancelar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void Txt_addd_Click(object sender, EventArgs e)
        {
            clase_Logicanegocio.SubirArchivos(lbl_nombre_doc.Text);
   
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tab_documento;

        }

        private void Txt_guardar_Click_1(object sender, EventArgs e)
        {
            guardar();
            //limpiar();
            buscar_datos("");
            tabControl1.SelectedTab = tab_mostrar;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            clase_Logicanegocio.SubirArchivos(lbl_ing.Text);
        }

        private void Btn_add_doc_Click(object sender, EventArgs e)
        {
            clase_Logicanegocio.SubirArchivos(lbl_doc.Text);
        }
    }
}
