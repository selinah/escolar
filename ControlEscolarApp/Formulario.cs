﻿using System;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class frm_usuarios : Form
    {
        UsuarioManejador _usuarioManejador;
        Usuarios _usuario;
        public frm_usuarios()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuarios();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BuscarDatos("");
        }
        private void BuscarDatos(string filtro)
        {

            dgvUsuarios.DataSource = _usuarioManejador.ObtenerLista(filtro);
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
            BuscarDatos(txt_buscar.Text);
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
                try
                {
                    eliminar();
                    BuscarDatos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(""+ex);
                }

        }
        private void eliminar()
        {
            int id = Convert.ToInt32(dgvUsuarios.CurrentRow.Cells["idusuario"].Value);
            _usuarioManejador.Eliminar(id);

        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            UsuarioModal _usuariomodal = new UsuarioModal();
            _usuariomodal.ShowDialog(); //Para NO poder cerrar la aplicación entera antes de haber cerrado usuariomodal
            BuscarDatos("");
        }

        private void dgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingUsuario();
            UsuarioModal us = new UsuarioModal(_usuario);
            us.ShowDialog();
            BuscarDatos("");
        }
        private void BindingUsuario()
        {
            if (true)
            {

            }

            _usuario.IdUsuario = Convert.ToInt32(dgvUsuarios.CurrentRow.Cells["idusuario"].Value);
            _usuario.Nombre = dgvUsuarios.CurrentRow.Cells["nombre"].Value.ToString();
            _usuario.ApellidoPaterno = dgvUsuarios.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            _usuario.ApellidoMaterno = dgvUsuarios.CurrentRow.Cells["apellidomaterno"].Value.ToString();

        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
