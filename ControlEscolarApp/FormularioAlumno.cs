﻿using System;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class FormularioAlumno : Form
    {
        AlumnoManejador _alumnoManejador;
        Alumnos _alumnos;
        public FormularioAlumno()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
        }

        private void FormularioAlumno_Load(object sender, EventArgs e)
        {
            BuscarDatos("");
        }
        private void BuscarDatos(string filtro)
        {

            dgvUsuarios.DataSource = _alumnoManejador.ObtenerLista(filtro);
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            AlumnoModal _alumnomodal = new AlumnoModal();
            _alumnomodal.ShowDialog(); //Para NO poder cerrar la aplicación entera antes de haber cerrado usuariomodal
            BuscarDatos("");
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
            BuscarDatos(txt_buscar.Text);
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                eliminar();
                BuscarDatos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
        }
        private void eliminar()
        {
            int id = Convert.ToInt32(dgvUsuarios.CurrentRow.Cells["ncontrol"].Value);
            _alumnoManejador.Eliminar(id);

        }

        private void dgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingUsuario();
            AlumnoModal _alumnomodal = new AlumnoModal(_alumnos);
            _alumnomodal.ShowDialog();
            BuscarDatos("");
        }

        private void BindingUsuario()
        {
            _alumnos.Ncontrol = Convert.ToInt32(dgvUsuarios.CurrentRow.Cells["ncontrol"].Value);
            _alumnos.Nombre = dgvUsuarios.CurrentRow.Cells["nombre"].Value.ToString();
            _alumnos.ApellidoPaterno = dgvUsuarios.CurrentRow.Cells["apellido_pat"].Value.ToString();
            _alumnos.ApellidoMaterno = dgvUsuarios.CurrentRow.Cells["apellido_mat"].Value.ToString();
            _alumnos.sexo = dgvUsuarios.CurrentRow.Cells["sexo"].Value.ToString();
            _alumnos.FechaNacimiento = dgvUsuarios.CurrentRow.Cells["fecha_nacimiento"].Value.ToString();
            _alumnos.Email = dgvUsuarios.CurrentRow.Cells["email"].Value.ToString();
            _alumnos.Telefono = dgvUsuarios.CurrentRow.Cells["telefono"].Value.ToString();
            _alumnos.estado = dgvUsuarios.CurrentRow.Cells["estado"].Value.ToString();
            _alumnos.municipio = dgvUsuarios.CurrentRow.Cells["municipio"].Value.ToString();
            _alumnos.Domicilio = dgvUsuarios.CurrentRow.Cells["domicilio"].Value.ToString();
            
        }
    }
}
