﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;
using Extension.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class FormularioEscuela : Form
    {
        private void FormularioEscuela_Load(object sender, EventArgs e)
        {
            btn_agregar_logo.Enabled = false;
            btn_eliminar_logo.Enabled = false;
            btn_guardar.Enabled = false;
            btn_cancelar.Enabled = false;
           
        }

        private EscuelaManejador _escuelaManejador;
        private OpenFileDialog _dialogCargarLogo;
        public bool _isImagenClear = false;
        private bool _isEnabledBinding = false;
        private Escuelas _escuela;
        private RutaManager _rutasManager;
       
        public FormularioEscuela()
        {
            InitializeComponent();
            _dialogCargarLogo = new OpenFileDialog();
            _rutasManager = new RutaManager(Application.StartupPath);
            _escuelaManejador = new EscuelaManejador(_rutasManager);
            _escuela = new Escuelas();
            if (string.IsNullOrEmpty(_escuela.IdEscuela.ToString()) ) //Comprobar si un parámatro está vacio o es nulo
            {
                LoadEntity();
            }
            _isEnabledBinding = true;
        }

        private void LoadEntity()
        {
            txt_nombre.Text = _escuela.Nombre;
            txt_director.Text = _escuela.Director;
            pic_logo.ImageLocation = null;

            if (!string.IsNullOrEmpty(_escuela.Logo) && string.IsNullOrEmpty(_dialogCargarLogo.FileName))
            {
                pic_logo.ImageLocation = _rutasManager.RutaLogoEscuela(_escuela);
            }
        }

        private void BingEntity()
        {
            _escuela.Nombre = txt_nombre.Text;
            _escuela.Director = txt_director.Text;
        }



        private void CargarLogo()
        {
            _dialogCargarLogo.Filter = "Imagen tipo (*.png)|*.png| Imagen Tipo (*.jpg)|*.jpg|";
            _dialogCargarLogo.Title = "Cargue un archivo";
            _dialogCargarLogo.ShowDialog();

            if (_dialogCargarLogo.FileName != "")
            {
                if (_escuelaManejador.CargarLogo(_dialogCargarLogo.FileName))
                {
                    pic_logo.ImageLocation = _dialogCargarLogo.FileName;
                    _isImagenClear = false;
                }
                else
                {
                    MessageBox.Show("Se debe de subir un archivo de menos de 5 MB");
                }
            }
        }
        private void Btn_agregar_logo_Click(object sender, EventArgs e)
        {
            CargarLogo();
        }

        private void Btn_eliminar_logo_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea eliminar?", "Eliminar", MessageBoxButtons.OKCancel)) == DialogResult.OK)
            {
                pic_logo.ImageLocation = null;
                _isImagenClear = true;
            } 
        }

        private void Txt_nombre_TextChanged(object sender, EventArgs e)
        {
            BingEntity();
        }

        private void Txt_director_TextChanged(object sender, EventArgs e)
        {
            BingEntity();
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            //ActivarCuadros(false);
            btn_agregar_logo.Enabled = false;
            btn_eliminar_logo.Enabled = false;
            btn_guardar.Enabled = false;
            btn_cancelar.Enabled = false;
            if ((MessageBox.Show("Seguro que desea Salir?", "Bruh", MessageBoxButtons.OKCancel)) == DialogResult.OK)
            {
                this.Close();
            }

        }

        private void Btn_modificar_Click(object sender, EventArgs e)
        {
            btn_agregar_logo.Enabled = true;
            btn_eliminar_logo.Enabled = true;
            btn_guardar.Enabled = true;
            btn_cancelar.Enabled = true;
        }

        public void Save()
        {
            try
            {
                if (pic_logo.Image != null)
                {
                    if (!string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                    {
                        _escuela.Logo = _escuelaManejador.GetNombreLogo(_dialogCargarLogo.FileName);
                    }
                    if (!string.IsNullOrEmpty(_escuela.Logo))
                    {
                        _escuelaManejador.GuardarLogo(_dialogCargarLogo.FileName, 1);
                        _dialogCargarLogo.Dispose();
                    }
                }
                else
                {
                    _escuela.Logo = string.Empty;
                }
                if (_isImagenClear)
                {
                    _escuelaManejador.LimpiarDoc(1, "png");
                    _escuelaManejador.LimpiarDoc(1, "jpg");
                }
                _escuelaManejador.guardar(_escuela);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        private void Btn_guardar_Click(object sender, EventArgs e)
        {
            Save();
        }
    }
}
