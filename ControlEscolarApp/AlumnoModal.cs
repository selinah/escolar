﻿using System;
using Entidades.ControlEscolarApp;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class AlumnoModal : Form
    {
        private AlumnoManejador _alumnoManejador;
        private Alumnos _alumnos;
        public AlumnoModal()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
        }

        public AlumnoModal(Alumnos alumno)
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
            _alumnos = alumno;
            loadentity();
        }

        private void loadentity()
        {
            txtNombre.Text = _alumnos.Nombre;
            txtApellidoPaterno.Text = _alumnos.ApellidoPaterno;
            txtApellidoMaterno.Text = _alumnos.ApellidoMaterno;
            cmb_genero.Text = _alumnos.sexo;
            txt_fecha_nac.Text = _alumnos.FechaNacimiento;
            txt_email.Text = _alumnos.Email;
            txt_telefono.Text = _alumnos.Telefono;
            cmb_estado.Text = _alumnos.estado;
            cmb_municipio.Text = _alumnos.municipio;
            txt_domicilio.Text = _alumnos.Domicilio;

        }

        private void AlumnoModal_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txt_Guardar_Click(object sender, EventArgs e)
        {
            string nom = txtNombre.Text.Trim();
            string apellpa = txtApellidoPaterno.Text.Trim();
            string apellma = txtApellidoMaterno.Text.Trim();
            try
            { 
                
                if (_alumnoManejador.NombreValido(txt_email.Text) == false)
                {
                    MessageBox.Show("Verifique la dirección de Correo");
                }
                else if (nom == "")
                {
                    MessageBox.Show("Verifique el nombre");
                }
                else if (apellpa == "")
                {
                    MessageBox.Show("Verifique el apellido paterno");
                }
                else if (apellma == "")
                {
                    MessageBox.Show("Verifique el apellido materno");
                }
                else
                {
                    Guardar();
                    this.Close();
                }

            }
            catch (Exception eee)
            {
                MessageBox.Show("Hubo un error: " + eee);
            }

        }
        void Guardar()
        {
            _alumnos.Nombre = txtNombre.Text+"";
            _alumnos.ApellidoPaterno = txtApellidoPaterno.Text+"";
            _alumnos.ApellidoMaterno = txtApellidoMaterno.Text+"";
            _alumnos.sexo = cmb_genero.Text+"";
            _alumnos.FechaNacimiento = txt_fecha_nac.Text+"";
            _alumnos.Email = txt_email.Text+"";
            _alumnos.Telefono = txt_telefono.Text+"";
            _alumnos.estado = cmb_estado.Text+"";
            _alumnos.municipio = cmb_municipio.Text+"";
            _alumnos.Domicilio = txt_domicilio.Text+"";
             _alumnoManejador.Guardar(_alumnos);
        }
    }
}
