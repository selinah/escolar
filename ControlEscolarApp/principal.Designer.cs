﻿namespace ControlEscolarApp
{
    partial class principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_catalogo = new System.Windows.Forms.Button();
            this.Alumnos = new System.Windows.Forms.Button();
            this.btn_escuelas = new System.Windows.Forms.Button();
            this.btn_maestros = new System.Windows.Forms.Button();
            this.btnrespaldo = new System.Windows.Forms.Button();
            this.btnmaterias = new System.Windows.Forms.Button();
            this.btngrupo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_catalogo
            // 
            this.btn_catalogo.Location = new System.Drawing.Point(25, 25);
            this.btn_catalogo.Name = "btn_catalogo";
            this.btn_catalogo.Size = new System.Drawing.Size(62, 23);
            this.btn_catalogo.TabIndex = 0;
            this.btn_catalogo.Text = "Catálogo";
            this.btn_catalogo.UseVisualStyleBackColor = true;
            this.btn_catalogo.Click += new System.EventHandler(this.btn_catalogo_Click);
            // 
            // Alumnos
            // 
            this.Alumnos.Location = new System.Drawing.Point(93, 25);
            this.Alumnos.Name = "Alumnos";
            this.Alumnos.Size = new System.Drawing.Size(59, 23);
            this.Alumnos.TabIndex = 1;
            this.Alumnos.Text = "Alumnos";
            this.Alumnos.UseVisualStyleBackColor = true;
            this.Alumnos.Click += new System.EventHandler(this.Alumnos_Click);
            // 
            // btn_escuelas
            // 
            this.btn_escuelas.Location = new System.Drawing.Point(226, 25);
            this.btn_escuelas.Name = "btn_escuelas";
            this.btn_escuelas.Size = new System.Drawing.Size(61, 23);
            this.btn_escuelas.TabIndex = 2;
            this.btn_escuelas.Text = "Escuelas";
            this.btn_escuelas.UseVisualStyleBackColor = true;
            this.btn_escuelas.Click += new System.EventHandler(this.Btn_escuelas_Click);
            // 
            // btn_maestros
            // 
            this.btn_maestros.Location = new System.Drawing.Point(158, 25);
            this.btn_maestros.Name = "btn_maestros";
            this.btn_maestros.Size = new System.Drawing.Size(62, 23);
            this.btn_maestros.TabIndex = 3;
            this.btn_maestros.Text = "Maestros";
            this.btn_maestros.UseVisualStyleBackColor = true;
            this.btn_maestros.Click += new System.EventHandler(this.Btn_maestros_Click);
            // 
            // btnrespaldo
            // 
            this.btnrespaldo.Location = new System.Drawing.Point(294, 25);
            this.btnrespaldo.Name = "btnrespaldo";
            this.btnrespaldo.Size = new System.Drawing.Size(75, 23);
            this.btnrespaldo.TabIndex = 4;
            this.btnrespaldo.Text = "Respaldo";
            this.btnrespaldo.UseVisualStyleBackColor = true;
            this.btnrespaldo.Click += new System.EventHandler(this.btnrespaldo_Click);
            // 
            // btnmaterias
            // 
            this.btnmaterias.Location = new System.Drawing.Point(375, 25);
            this.btnmaterias.Name = "btnmaterias";
            this.btnmaterias.Size = new System.Drawing.Size(75, 23);
            this.btnmaterias.TabIndex = 5;
            this.btnmaterias.Text = "Materias";
            this.btnmaterias.UseVisualStyleBackColor = true;
            this.btnmaterias.Click += new System.EventHandler(this.btnmaterias_Click);
            // 
            // btngrupo
            // 
            this.btngrupo.Location = new System.Drawing.Point(457, 25);
            this.btngrupo.Name = "btngrupo";
            this.btngrupo.Size = new System.Drawing.Size(75, 23);
            this.btngrupo.TabIndex = 6;
            this.btngrupo.Text = "Grupo";
            this.btngrupo.UseVisualStyleBackColor = true;
            this.btngrupo.Click += new System.EventHandler(this.btngrupo_Click);
            // 
            // principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 82);
            this.Controls.Add(this.btngrupo);
            this.Controls.Add(this.btnmaterias);
            this.Controls.Add(this.btnrespaldo);
            this.Controls.Add(this.btn_maestros);
            this.Controls.Add(this.btn_escuelas);
            this.Controls.Add(this.Alumnos);
            this.Controls.Add(this.btn_catalogo);
            this.Name = "principal";
            this.Text = "2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_catalogo;
        private System.Windows.Forms.Button Alumnos;
        private System.Windows.Forms.Button btn_escuelas;
        private System.Windows.Forms.Button btn_maestros;
        private System.Windows.Forms.Button btnrespaldo;
        private System.Windows.Forms.Button btnmaterias;
        private System.Windows.Forms.Button btngrupo;
    }
}