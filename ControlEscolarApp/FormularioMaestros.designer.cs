﻿namespace ControlEscolarApp
{
    partial class FormularioMaestros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_mostrar = new System.Windows.Forms.TabPage();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.visor = new System.Windows.Forms.DataGridView();
            this.tab_insertar = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.cmb_sexo = new System.Windows.Forms.ComboBox();
            this.cmb_municipio = new System.Windows.Forms.ComboBox();
            this.cmb_estado = new System.Windows.Forms.ComboBox();
            this.txt_cancelar = new System.Windows.Forms.Button();
            this.txt_no_tarjeta = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_cooreo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_nacimiento = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_amat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_apat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_no_control = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tab_documento = new System.Windows.Forms.TabPage();
            this.txt_guardar = new System.Windows.Forms.Button();
            this.btn_add_doc = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.lbl_doc = new System.Windows.Forms.Label();
            this.btn_addd_ing = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lbl_ing = new System.Windows.Forms.Label();
            this.txt_addd = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_nombre_doc = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editarDocumentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarRegistroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tab_mostrar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visor)).BeginInit();
            this.tab_insertar.SuspendLayout();
            this.tab_documento.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_mostrar);
            this.tabControl1.Controls.Add(this.tab_insertar);
            this.tabControl1.Controls.Add(this.tab_documento);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(672, 229);
            this.tabControl1.TabIndex = 0;
            // 
            // tab_mostrar
            // 
            this.tab_mostrar.Controls.Add(this.textBox11);
            this.tab_mostrar.Controls.Add(this.label11);
            this.tab_mostrar.Controls.Add(this.visor);
            this.tab_mostrar.Location = new System.Drawing.Point(4, 22);
            this.tab_mostrar.Name = "tab_mostrar";
            this.tab_mostrar.Padding = new System.Windows.Forms.Padding(3);
            this.tab_mostrar.Size = new System.Drawing.Size(664, 203);
            this.tab_mostrar.TabIndex = 0;
            this.tab_mostrar.Text = "Mostrar lista";
            this.tab_mostrar.UseVisualStyleBackColor = true;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(220, 157);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(306, 20);
            this.textBox11.TabIndex = 2;
            this.textBox11.TextChanged += new System.EventHandler(this.TextBox11_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(206, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Escriba para buscar su nombre en la lista: ";
            // 
            // visor
            // 
            this.visor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.visor.Location = new System.Drawing.Point(6, 3);
            this.visor.Name = "visor";
            this.visor.Size = new System.Drawing.Size(620, 133);
            this.visor.TabIndex = 0;
            this.visor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Visor_CellContentClick);
            this.visor.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Visor_CellMouseClick);
            // 
            // tab_insertar
            // 
            this.tab_insertar.Controls.Add(this.button3);
            this.tab_insertar.Controls.Add(this.cmb_sexo);
            this.tab_insertar.Controls.Add(this.cmb_municipio);
            this.tab_insertar.Controls.Add(this.cmb_estado);
            this.tab_insertar.Controls.Add(this.txt_cancelar);
            this.tab_insertar.Controls.Add(this.txt_no_tarjeta);
            this.tab_insertar.Controls.Add(this.label10);
            this.tab_insertar.Controls.Add(this.label9);
            this.tab_insertar.Controls.Add(this.txt_cooreo);
            this.tab_insertar.Controls.Add(this.label8);
            this.tab_insertar.Controls.Add(this.label7);
            this.tab_insertar.Controls.Add(this.label6);
            this.tab_insertar.Controls.Add(this.txt_nacimiento);
            this.tab_insertar.Controls.Add(this.label5);
            this.tab_insertar.Controls.Add(this.txt_amat);
            this.tab_insertar.Controls.Add(this.label4);
            this.tab_insertar.Controls.Add(this.txt_apat);
            this.tab_insertar.Controls.Add(this.label3);
            this.tab_insertar.Controls.Add(this.txt_nombre);
            this.tab_insertar.Controls.Add(this.label2);
            this.tab_insertar.Controls.Add(this.txt_no_control);
            this.tab_insertar.Controls.Add(this.label1);
            this.tab_insertar.Location = new System.Drawing.Point(4, 22);
            this.tab_insertar.Name = "tab_insertar";
            this.tab_insertar.Padding = new System.Windows.Forms.Padding(3);
            this.tab_insertar.Size = new System.Drawing.Size(664, 203);
            this.tab_insertar.TabIndex = 1;
            this.tab_insertar.Text = "Insercción de Datos";
            this.tab_insertar.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(525, 135);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "Siguiente";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // cmb_sexo
            // 
            this.cmb_sexo.FormattingEnabled = true;
            this.cmb_sexo.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.cmb_sexo.Location = new System.Drawing.Point(390, 108);
            this.cmb_sexo.Name = "cmb_sexo";
            this.cmb_sexo.Size = new System.Drawing.Size(100, 21);
            this.cmb_sexo.TabIndex = 16;
            // 
            // cmb_municipio
            // 
            this.cmb_municipio.FormattingEnabled = true;
            this.cmb_municipio.Location = new System.Drawing.Point(390, 58);
            this.cmb_municipio.Name = "cmb_municipio";
            this.cmb_municipio.Size = new System.Drawing.Size(100, 21);
            this.cmb_municipio.TabIndex = 12;
            // 
            // cmb_estado
            // 
            this.cmb_estado.FormattingEnabled = true;
            this.cmb_estado.Location = new System.Drawing.Point(390, 31);
            this.cmb_estado.Name = "cmb_estado";
            this.cmb_estado.Size = new System.Drawing.Size(100, 21);
            this.cmb_estado.TabIndex = 11;
            // 
            // txt_cancelar
            // 
            this.txt_cancelar.Location = new System.Drawing.Point(525, 164);
            this.txt_cancelar.Name = "txt_cancelar";
            this.txt_cancelar.Size = new System.Drawing.Size(75, 23);
            this.txt_cancelar.TabIndex = 21;
            this.txt_cancelar.Text = "Cancelar";
            this.txt_cancelar.UseVisualStyleBackColor = true;
            this.txt_cancelar.Click += new System.EventHandler(this.Txt_cancelar_Click);
            // 
            // txt_no_tarjeta
            // 
            this.txt_no_tarjeta.Location = new System.Drawing.Point(390, 135);
            this.txt_no_tarjeta.Name = "txt_no_tarjeta";
            this.txt_no_tarjeta.Size = new System.Drawing.Size(100, 20);
            this.txt_no_tarjeta.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(276, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Numero de seguro";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(276, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Sexo";
            // 
            // txt_cooreo
            // 
            this.txt_cooreo.Location = new System.Drawing.Point(390, 83);
            this.txt_cooreo.Name = "txt_cooreo";
            this.txt_cooreo.Size = new System.Drawing.Size(100, 20);
            this.txt_cooreo.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(276, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Correo Electrónico";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(276, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Municipio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(276, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Estado";
            // 
            // txt_nacimiento
            // 
            this.txt_nacimiento.Location = new System.Drawing.Point(145, 135);
            this.txt_nacimiento.Name = "txt_nacimiento";
            this.txt_nacimiento.Size = new System.Drawing.Size(100, 20);
            this.txt_nacimiento.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label5.Location = new System.Drawing.Point(17, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "FN dia-mes-anio";
            // 
            // txt_amat
            // 
            this.txt_amat.Location = new System.Drawing.Point(145, 109);
            this.txt_amat.Name = "txt_amat";
            this.txt_amat.Size = new System.Drawing.Size(100, 20);
            this.txt_amat.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Apellido Materno";
            // 
            // txt_apat
            // 
            this.txt_apat.Location = new System.Drawing.Point(145, 83);
            this.txt_apat.Name = "txt_apat";
            this.txt_apat.Size = new System.Drawing.Size(100, 20);
            this.txt_apat.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Apellido Paterno";
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(145, 57);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(100, 20);
            this.txt_nombre.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre";
            // 
            // txt_no_control
            // 
            this.txt_no_control.Location = new System.Drawing.Point(145, 31);
            this.txt_no_control.Name = "txt_no_control";
            this.txt_no_control.Size = new System.Drawing.Size(100, 20);
            this.txt_no_control.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número de Control";
            // 
            // tab_documento
            // 
            this.tab_documento.Controls.Add(this.txt_guardar);
            this.tab_documento.Controls.Add(this.btn_add_doc);
            this.tab_documento.Controls.Add(this.label15);
            this.tab_documento.Controls.Add(this.lbl_doc);
            this.tab_documento.Controls.Add(this.btn_addd_ing);
            this.tab_documento.Controls.Add(this.label13);
            this.tab_documento.Controls.Add(this.lbl_ing);
            this.tab_documento.Controls.Add(this.txt_addd);
            this.tab_documento.Controls.Add(this.label12);
            this.tab_documento.Controls.Add(this.lbl_nombre_doc);
            this.tab_documento.Location = new System.Drawing.Point(4, 22);
            this.tab_documento.Name = "tab_documento";
            this.tab_documento.Size = new System.Drawing.Size(664, 203);
            this.tab_documento.TabIndex = 2;
            this.tab_documento.Text = "Documentación";
            this.tab_documento.UseVisualStyleBackColor = true;
            // 
            // txt_guardar
            // 
            this.txt_guardar.Location = new System.Drawing.Point(469, 150);
            this.txt_guardar.Name = "txt_guardar";
            this.txt_guardar.Size = new System.Drawing.Size(75, 23);
            this.txt_guardar.TabIndex = 21;
            this.txt_guardar.Text = "Guardar";
            this.txt_guardar.UseVisualStyleBackColor = true;
            this.txt_guardar.Click += new System.EventHandler(this.Txt_guardar_Click_1);
            // 
            // btn_add_doc
            // 
            this.btn_add_doc.Location = new System.Drawing.Point(286, 87);
            this.btn_add_doc.Name = "btn_add_doc";
            this.btn_add_doc.Size = new System.Drawing.Size(75, 23);
            this.btn_add_doc.TabIndex = 8;
            this.btn_add_doc.Text = "Agregar";
            this.btn_add_doc.UseVisualStyleBackColor = true;
            this.btn_add_doc.Click += new System.EventHandler(this.Btn_add_doc_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(50, 92);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(228, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Agregue el nombre del archivo para Doctorado";
            // 
            // lbl_doc
            // 
            this.lbl_doc.AutoSize = true;
            this.lbl_doc.Location = new System.Drawing.Point(383, 92);
            this.lbl_doc.Name = "lbl_doc";
            this.lbl_doc.Size = new System.Drawing.Size(46, 13);
            this.lbl_doc.TabIndex = 6;
            this.lbl_doc.Text = "<Vacio>";
            // 
            // btn_addd_ing
            // 
            this.btn_addd_ing.Location = new System.Drawing.Point(286, 58);
            this.btn_addd_ing.Name = "btn_addd_ing";
            this.btn_addd_ing.Size = new System.Drawing.Size(75, 23);
            this.btn_addd_ing.TabIndex = 5;
            this.btn_addd_ing.Text = "Agregar";
            this.btn_addd_ing.UseVisualStyleBackColor = true;
            this.btn_addd_ing.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(50, 63);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(227, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Agregue el nombre del archivo para Ingenietria";
            // 
            // lbl_ing
            // 
            this.lbl_ing.AutoSize = true;
            this.lbl_ing.Location = new System.Drawing.Point(383, 63);
            this.lbl_ing.Name = "lbl_ing";
            this.lbl_ing.Size = new System.Drawing.Size(46, 13);
            this.lbl_ing.TabIndex = 3;
            this.lbl_ing.Text = "<Vacio>";
            // 
            // txt_addd
            // 
            this.txt_addd.Location = new System.Drawing.Point(286, 29);
            this.txt_addd.Name = "txt_addd";
            this.txt_addd.Size = new System.Drawing.Size(75, 23);
            this.txt_addd.TabIndex = 2;
            this.txt_addd.Text = "Agregar";
            this.txt_addd.UseVisualStyleBackColor = true;
            this.txt_addd.Click += new System.EventHandler(this.Txt_addd_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(50, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(218, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Agregue el nombre del archivo para Maestria";
            // 
            // lbl_nombre_doc
            // 
            this.lbl_nombre_doc.AutoSize = true;
            this.lbl_nombre_doc.Location = new System.Drawing.Point(383, 34);
            this.lbl_nombre_doc.Name = "lbl_nombre_doc";
            this.lbl_nombre_doc.Size = new System.Drawing.Size(46, 13);
            this.lbl_nombre_doc.TabIndex = 0;
            this.lbl_nombre_doc.Text = "<Vacio>";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarDocumentoToolStripMenuItem,
            this.editarRegistroToolStripMenuItem,
            this.eliminarToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(215, 70);
            // 
            // editarDocumentoToolStripMenuItem
            // 
            this.editarDocumentoToolStripMenuItem.Name = "editarDocumentoToolStripMenuItem";
            this.editarDocumentoToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.editarDocumentoToolStripMenuItem.Text = "Insertar/Editar Documento";
            this.editarDocumentoToolStripMenuItem.Click += new System.EventHandler(this.EditarDocumentoToolStripMenuItem_Click);
            // 
            // editarRegistroToolStripMenuItem
            // 
            this.editarRegistroToolStripMenuItem.Name = "editarRegistroToolStripMenuItem";
            this.editarRegistroToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.editarRegistroToolStripMenuItem.Text = "Editar Registro";
            this.editarRegistroToolStripMenuItem.Click += new System.EventHandler(this.EditarRegistroToolStripMenuItem_Click);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            this.eliminarToolStripMenuItem.Click += new System.EventHandler(this.EliminarToolStripMenuItem_Click);
            // 
            // FormularioMaestros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 254);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormularioMaestros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control escolar para maestros";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tab_mostrar.ResumeLayout(false);
            this.tab_mostrar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visor)).EndInit();
            this.tab_insertar.ResumeLayout(false);
            this.tab_insertar.PerformLayout();
            this.tab_documento.ResumeLayout(false);
            this.tab_documento.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_mostrar;
        private System.Windows.Forms.DataGridView visor;
        private System.Windows.Forms.TabPage tab_insertar;
        private System.Windows.Forms.TabPage tab_documento;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button txt_cancelar;
        private System.Windows.Forms.TextBox txt_no_tarjeta;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_nacimiento;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_amat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_apat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_no_control;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editarDocumentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarRegistroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ComboBox cmb_estado;
        private System.Windows.Forms.ComboBox cmb_sexo;
        private System.Windows.Forms.ComboBox cmb_municipio;
        private System.Windows.Forms.TextBox txt_cooreo;
        private System.Windows.Forms.Label lbl_nombre_doc;
        private System.Windows.Forms.Button txt_addd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button txt_guardar;
        private System.Windows.Forms.Button btn_add_doc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbl_doc;
        private System.Windows.Forms.Button btn_addd_ing;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_ing;
    }
}

