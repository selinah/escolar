﻿namespace ControlEscolarApp
{
    partial class Respaldo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnbuscarruta = new System.Windows.Forms.Button();
            this.btncrear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.asincronoguardar = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // btnbuscarruta
            // 
            this.btnbuscarruta.Location = new System.Drawing.Point(56, 35);
            this.btnbuscarruta.Name = "btnbuscarruta";
            this.btnbuscarruta.Size = new System.Drawing.Size(75, 23);
            this.btnbuscarruta.TabIndex = 0;
            this.btnbuscarruta.Text = "Buscar ruta";
            this.btnbuscarruta.UseVisualStyleBackColor = true;
            this.btnbuscarruta.Click += new System.EventHandler(this.btnbuscarruta_Click);
            // 
            // btncrear
            // 
            this.btncrear.Location = new System.Drawing.Point(223, 34);
            this.btncrear.Name = "btncrear";
            this.btncrear.Size = new System.Drawing.Size(75, 23);
            this.btncrear.TabIndex = 1;
            this.btncrear.Text = "Crear respaldo";
            this.btncrear.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // asincronoguardar
            // 
            this.asincronoguardar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.asincronoguardar_DoWork);
            this.asincronoguardar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.asincronoguardar_RunWorkerCompleted);
            // 
            // Respaldo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 196);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btncrear);
            this.Controls.Add(this.btnbuscarruta);
            this.Name = "Respaldo";
            this.Text = "Respaldo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnbuscarruta;
        private System.Windows.Forms.Button btncrear;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker asincronoguardar;
    }
}