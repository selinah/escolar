﻿namespace ControlEscolarApp
{
    partial class FormularioEscuela
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.txt_director = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pic_logo = new System.Windows.Forms.PictureBox();
            this.btn_agregar_logo = new System.Windows.Forms.Button();
            this.btn_eliminar_logo = new System.Windows.Forms.Button();
            this.btn_modificar = new System.Windows.Forms.Button();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // txt_nombre
            // 
            this.txt_nombre.Enabled = false;
            this.txt_nombre.Location = new System.Drawing.Point(26, 41);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(141, 20);
            this.txt_nombre.TabIndex = 1;
            this.txt_nombre.TextChanged += new System.EventHandler(this.Txt_nombre_TextChanged);
            // 
            // txt_director
            // 
            this.txt_director.Enabled = false;
            this.txt_director.Location = new System.Drawing.Point(26, 102);
            this.txt_director.Name = "txt_director";
            this.txt_director.Size = new System.Drawing.Size(141, 20);
            this.txt_director.TabIndex = 3;
            this.txt_director.TextChanged += new System.EventHandler(this.Txt_director_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Director";
            // 
            // pic_logo
            // 
            this.pic_logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_logo.Location = new System.Drawing.Point(182, 32);
            this.pic_logo.Name = "pic_logo";
            this.pic_logo.Size = new System.Drawing.Size(187, 119);
            this.pic_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_logo.TabIndex = 4;
            this.pic_logo.TabStop = false;
            // 
            // btn_agregar_logo
            // 
            this.btn_agregar_logo.Enabled = false;
            this.btn_agregar_logo.Location = new System.Drawing.Point(375, 58);
            this.btn_agregar_logo.Name = "btn_agregar_logo";
            this.btn_agregar_logo.Size = new System.Drawing.Size(75, 23);
            this.btn_agregar_logo.TabIndex = 5;
            this.btn_agregar_logo.Text = "Agregar foto";
            this.btn_agregar_logo.UseVisualStyleBackColor = true;
            this.btn_agregar_logo.Click += new System.EventHandler(this.Btn_agregar_logo_Click);
            // 
            // btn_eliminar_logo
            // 
            this.btn_eliminar_logo.Enabled = false;
            this.btn_eliminar_logo.Location = new System.Drawing.Point(375, 99);
            this.btn_eliminar_logo.Name = "btn_eliminar_logo";
            this.btn_eliminar_logo.Size = new System.Drawing.Size(75, 23);
            this.btn_eliminar_logo.TabIndex = 6;
            this.btn_eliminar_logo.Text = "Quitar Foto";
            this.btn_eliminar_logo.UseVisualStyleBackColor = true;
            this.btn_eliminar_logo.Click += new System.EventHandler(this.Btn_eliminar_logo_Click);
            // 
            // btn_modificar
            // 
            this.btn_modificar.Location = new System.Drawing.Point(247, 165);
            this.btn_modificar.Name = "btn_modificar";
            this.btn_modificar.Size = new System.Drawing.Size(58, 23);
            this.btn_modificar.TabIndex = 7;
            this.btn_modificar.Text = "Modificar";
            this.btn_modificar.UseVisualStyleBackColor = true;
            this.btn_modificar.Click += new System.EventHandler(this.Btn_modificar_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.Enabled = false;
            this.btn_guardar.Location = new System.Drawing.Point(182, 165);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(59, 23);
            this.btn_guardar.TabIndex = 8;
            this.btn_guardar.Text = "Guardar";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.Btn_guardar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Enabled = false;
            this.btn_cancelar.Location = new System.Drawing.Point(310, 165);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(59, 23);
            this.btn_cancelar.TabIndex = 9;
            this.btn_cancelar.Text = "Eliminar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click);
            // 
            // FormularioEscuela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 211);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.btn_modificar);
            this.Controls.Add(this.btn_eliminar_logo);
            this.Controls.Add(this.btn_agregar_logo);
            this.Controls.Add(this.pic_logo);
            this.Controls.Add(this.txt_director);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_nombre);
            this.Controls.Add(this.label1);
            this.Name = "FormularioEscuela";
            this.Text = "FormularioEscuela";
            this.Load += new System.EventHandler(this.FormularioEscuela_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.TextBox txt_director;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pic_logo;
        private System.Windows.Forms.Button btn_agregar_logo;
        private System.Windows.Forms.Button btn_eliminar_logo;
        private System.Windows.Forms.Button btn_modificar;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Button btn_cancelar;
    }
}