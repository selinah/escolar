﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Extension.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class Respaldo : Form
    {
        RespaldoManejador nuevo;
        RutaManager otro;
        string donde;
        bool resultado;
        public Respaldo()
        {
            InitializeComponent();
            otro = new RutaManager(Application.StartupPath);
            nuevo = new RespaldoManejador(otro);
            btncrear.Enabled = false;
        }

        private void btnbuscarruta_Click(object sender, EventArgs e)
        {
            btncrear.Enabled = true;
            using (var fd=new FolderBrowserDialog())
            {
                if (fd.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fd.SelectedPath)) 
                {
                    donde = fd.SelectedPath;
                    label1.Text = donde;
                }
            }
        }
        private void crear()
        {
            resultado = nuevo.respaldo(donde);
        }

        private void asincronoguardar_DoWork(object sender, DoWorkEventArgs e)
        {
            crear();
        }

        private void asincronoguardar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Guardado");
        }
    }
}
