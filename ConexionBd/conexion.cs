﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace ConexionBd
{
    public class conexion
    {
         MySqlConnection _con;
        public conexion(string server, string user, string password, string database, uint port)
        {
            MySqlConnectionStringBuilder cadena = new MySqlConnectionStringBuilder();
            cadena.Server = server;
            cadena.UserID = user;
            cadena.Password = password;
            cadena.Database = database;
            cadena.Port = port;
            _con = new MySqlConnection(cadena.ToString());
        }

        public void ejecutarconsulta(string cadena)
        {
            _con.Open();
            MySqlCommand cmm = new MySqlCommand(cadena, _con);
            cmm.ExecuteNonQuery();
            _con.Close();
        }
        public DataSet ObtenerDatos(string cadena, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(cadena, _con);
            da.Fill(ds, tabla);
            return ds;
        }
    }
}
