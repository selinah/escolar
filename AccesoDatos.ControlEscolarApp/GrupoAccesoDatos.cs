﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolarApp
{
    public class GrupoAccesoDatos
    {
            conexion _conexion;
            public GrupoAccesoDatos()
            {
                _conexion = new conexion(
                    "localhost",
                    "root",
                    "",
                    "escolar",
                    3306);
            }
            public void Eliminar(int IdGrupo)
            {
                string cadena = string.Format("delete from grupos where idGrupo = {0} ", IdGrupo);
                _conexion.ejecutarconsulta(cadena);
            }

            public void Guardar(Grupo grupo)
            {
                if (grupo.IdGrupo == 0)
                {
                    string cadena = string.Format("insert into grupos VALUES(null, '{0}', '{1}', '{2}', " +
                        " '{3}', '{4}')",
                        grupo.Nombre,
                        grupo.Fkmateria,
                        grupo.Carrera);
                    _conexion.ejecutarconsulta(cadena);
                }
                else
                {
                    string cadena = string.Format("update grupo set nombre = '{0}', Fkmateria = '{1}', carrera = '{2}' ;",
                        grupo.Nombre,
                        grupo.Fkmateria,
                        grupo.Carrera);
                _conexion.ejecutarconsulta(cadena);
                }
            }

            public List<Grupo> ObtenerLista(string filtro)
            {
                var list = new List<Grupo>();
                string consulta = String.Format("select * from grupos where nombre like '%{0}%'", filtro);
                var ds = _conexion.ObtenerDatos(consulta, "grupos");
                var dt = ds.Tables[0];

                foreach (DataRow row in dt.Rows)
                {
                    var Grupo = new Grupo
                    {
                        IdGrupo= Convert.ToInt32(row["Idgrupo"]),
                        Nombre = row["nombre"].ToString(),
                        Fkmateria = row["materias"].ToString(),
                        Carrera = row["carrera"].ToString(),
                    };
                    list.Add(Grupo);
                }
                return list;
            }

        }
    }


