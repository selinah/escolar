﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolarApp
{
    public class EscuelasAccesoDatos
    {
        conexion _conexion;
        public EscuelasAccesoDatos()
        {
            _conexion = new conexion(
                "localhost",
                "root",
                "",
                "escolar",
                3306);
        }
        public void guardar(Escuelas escuela)
        {
            if (escuela.IdEscuela == 0)
            {
                string consulta = string.Format("insert into escuela values (null, '{0}' ,' {1}', '{2}');",
                    escuela.Nombre,
                    escuela.Director,
                    escuela.Logo);
                _conexion.ejecutarconsulta(consulta);
            }

            else
            {
                string consulta = String.Format("update escuela set nombre = {0}, director = {1}, logo = {2} where idescuela = {3}",
                    escuela.Nombre,
                    escuela.Director,
                    escuela.Logo,
                    escuela.IdEscuela);
            }
        } 

        public Escuelas GetEscuelas(string filtro)
        {
            var ds = new DataSet();
            string consulta = String.Format("select * from escuela where nombre like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "datos");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var escuela = new Escuelas();

            foreach (DataRow row in dt.Rows)
            {
                escuela.IdEscuela = Convert.ToInt32(row["idEscuela"]);
                escuela.Nombre = row["nombre"].ToString();
                escuela.Director = row["director"].ToString();
                escuela.Logo = row["logo"].ToString();

            }
            return escuela;
        }
    }
}
