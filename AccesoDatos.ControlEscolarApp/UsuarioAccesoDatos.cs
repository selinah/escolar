﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;

using System.Data;

namespace AccesoDatos.ControlEscolarApp
{
    public class UsuarioAccesoDatos  
    {
        conexion _conexion;
        public UsuarioAccesoDatos()
        {
            _conexion = new conexion(
                "localhost",
                "root",
                "",
                "escolar",
                3306);
        }
        public void Eliminar(int idUsuario)
        {
            string cadena = string.Format("delete from usuario where idusuario = {0} ", idUsuario);
            _conexion.ejecutarconsulta(cadena);
        }

        public void Guardar(Usuarios usuarios)
        {
            if (usuarios.IdUsuario == 0)
            {
                string cadena = string.Format("insert into usuario values(null,'{0}','{1}','{2}')",
                    usuarios.Nombre,
                    usuarios.ApellidoPaterno,
                    usuarios.ApellidoMaterno);
                _conexion.ejecutarconsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update usuario set nombre ='{0}', apellidopaterno = '{1}',apellidomaterno ='{2}' " +
                    "where idusuario = {3} ;",
                    usuarios.Nombre,
                    usuarios.ApellidoPaterno,
                    usuarios.ApellidoMaterno,
                    usuarios.IdUsuario);
                _conexion.ejecutarconsulta(cadena);
            }
            

        }

        public List<Usuarios> ObtenerLista(string filtro)
        {
            var list = new List<Usuarios>();
            string consulta = String.Format( "select * from usuario where nombre like '%{0}%'",filtro);
            var ds = _conexion.ObtenerDatos(consulta, "usuario");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var usuario = new Usuarios
                {
                    IdUsuario = Convert.ToInt32(row["idusuario"]),
                    Nombre = row["nombre"].ToString(),
                    ApellidoPaterno = row["apellidopaterno"].ToString(),
                    ApellidoMaterno = row["apellidomaterno"].ToString()
                };
                list.Add(usuario);
            }
            return list;
        }
    }
}
