﻿using System;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AccesoDatos.ControlEscolarApp 
{
    public class clase_accesodatos
    {
        conexion _conexion;
        public clase_accesodatos()
        {
            _conexion = new conexion(
                "localhost",
                "root",
                "",
                "escolar",
                3306);
        }
        public void Eliminar(Maestros lista_Entidades)
        {
            string cadena = string.Format("delete from maestros where no_control = '{0}'",
                lista_Entidades.No_control);
            _conexion.ejecutarconsulta(cadena);
        }

        public void Guardar(Maestros lista_Entidades)
        {
            if (lista_Entidades.No_control != "")
            {
                string cadena = string.Format("insert into maestros values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}');",
                lista_Entidades.No_control,
                lista_Entidades.Nombre,
                lista_Entidades.Apellido_paterno,
                lista_Entidades.Apellido_materno,
                lista_Entidades.Fecha_nacimiento,
                lista_Entidades.Estado,
                lista_Entidades.Municipio,
                lista_Entidades.Sexo,
                lista_Entidades.Email,
                lista_Entidades.No_tarjeta,
                lista_Entidades.Nombre_doc
                );
                _conexion.ejecutarconsulta(cadena);
            }
            else
            {

            }
        }

        public void modificar(Maestros lista_Entidades)
        {
                string cadena = string.Format("update maestros set nombre = '{0}', apellido_paterno = '{1}', apellido_materno = '{2}', fecha_nacimiento = '{3}', " +
                "estado = '{4}', municipio = '{5}', sexo = '{6}', email ='{7}', no_tarjeta = '{8}', nombre_doc = '{9}' where no_control = {10} ;",

            lista_Entidades.Nombre,
            lista_Entidades.Apellido_paterno,
            lista_Entidades.Apellido_materno,
            lista_Entidades.Fecha_nacimiento,
            lista_Entidades.Estado,
            lista_Entidades.Municipio,
            lista_Entidades.Sexo,
            lista_Entidades.Email,
            lista_Entidades.No_tarjeta,
            lista_Entidades.Nombre_doc,
            lista_Entidades.No_control
            );
            _conexion.ejecutarconsulta(cadena);
        }

        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            string consulta = String.Format("select * from maestros where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestros");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var lista_entidades = new Maestros
                {
                    No_control = row["no_control"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellido_paterno = row["apellido_paterno"].ToString(),
                    Apellido_materno = row["apellido_materno"].ToString(),
                    Fecha_nacimiento = row["fecha_nacimiento"].ToString(),
                    Estado = row["estado"].ToString(),
                    Municipio = row["municipio"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Email = row["email"].ToString(),
                    No_tarjeta = row["no_tarjeta"].ToString(),
                    Nombre_doc = row["nombre_doc"].ToString()
                };
                list.Add(lista_entidades);
            }
            return list;
        }
    }
}
