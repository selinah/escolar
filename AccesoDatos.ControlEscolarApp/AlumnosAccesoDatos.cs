﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolarApp
{
    public class AlumnosAccesoDatos
    {
        conexion _conexion;
        public AlumnosAccesoDatos()
        {
            _conexion = new conexion(
                "localhost",
                "root",
                "",
                "escolar",
                3306);
        }
        public void Eliminar(int num_control)
        {
            string cadena = string.Format("delete from alumnos where ncontrol = {0} ", num_control);
            _conexion.ejecutarconsulta(cadena);
        }

        public void Guardar(Alumnos alumnos)
        {
            if (alumnos.Ncontrol == 0)
            {
                string cadena = string.Format("insert into alumnos VALUES(null, '{0}', '{1}', '{2}', " +
                    " '{3}', '{4}' , '{5}' , '{6}', '{7}' , '{8}' , '{9}')",
                    alumnos.Nombre,
                    alumnos.ApellidoPaterno,
                    alumnos.ApellidoMaterno,
                    alumnos.sexo,
                    alumnos.FechaNacimiento,
                    alumnos.Email,
                    alumnos.Telefono,
                    alumnos.estado,
                    alumnos.municipio,
                    alumnos.Domicilio);
                _conexion.ejecutarconsulta(cadena); 
            }
            else
            {
                string cadena = string.Format("update alumnos set Nombre = '{0}', ApellidoPaterno = '{1}', ApellidoMaterno = '{2}', sexo = '{3}', " +
                    "fechaNacimiento = '{4}', email = '{5}', telefono = '{6}', estado ='{7}', municipio = '{8}', domicilio = '{9}' where Ncontrol = {10} ;",
                    alumnos.Nombre,
                    alumnos.ApellidoPaterno,
                    alumnos.ApellidoMaterno,
                    alumnos.sexo,
                    alumnos.FechaNacimiento,
                    alumnos.Email,
                    alumnos.Telefono,
                    alumnos.estado,
                    alumnos.municipio,
                    alumnos.Domicilio);
                _conexion.ejecutarconsulta(cadena);
            }
        }

        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            string consulta = String.Format("select * from alumnos where Nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "alumnos");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumnos
                {
                    Ncontrol = Convert.ToInt32(row["ncontrol"]),
                    Nombre = row["nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    sexo = row["sexo"].ToString(),
                    FechaNacimiento = row["FechaNacimiento"].ToString(),
                    Email = row["email"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    estado = row["estado"].ToString(),
                    municipio = row["municipio"].ToString(),
                    Domicilio = row["domicilio"].ToString()
                };
                list.Add(alumno);
            }
            return list;
        }

    }
}
