﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolarApp
{
    public class MateriasAccesoDatos
    {
        conexion _conexion;
        public MateriasAccesoDatos()
        {
            _conexion = new conexion(
                "localhost",
                "root",
                "",
                "escolar",
                3306);
        }
        public void Eliminar(int IdMateria)
        {
            string cadena = string.Format("delete from materias where ncontrol = {0} ", IdMateria);
            _conexion.ejecutarconsulta(cadena);
        }

        public void Guardar(Materias materias)
        {
            if (materias.IdMateria == 0)
            {
                string cadena = string.Format("insert into materias VALUES(null, '{0}', '{1}', '{2}', " +
                    " '{3}', '{4}')",
                   materias.Nombre,
                    materias.Materiaantecesor,
                    materias.Materiapredecesor,
                    materias.Carrera);
                _conexion.ejecutarconsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update materias set nombre = '{0}', Materiaantecesor = '{1}', Materiapredecesor = '{2}', carrera = '{3}' ;",
                    materias.Nombre,
                    materias.Materiaantecesor,
                    materias.Materiapredecesor,
                    materias.Carrera);
                _conexion.ejecutarconsulta(cadena);
            }
        }

        public List<Materias> ObtenerLista(string filtro)
        {
            var list = new List<Materias>();
            string consulta = String.Format("select * from materias where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "materias");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var Materias = new Materias
                {
                    IdMateria = Convert.ToInt32(row["Idmatera"]),
                    Nombre = row["nombre"].ToString(),
                    Materiaantecesor = row["materiaantecesor"].ToString(),
                    Materiapredecesor= row["materiapredecesor"].ToString(),
                    Carrera= row["carrera"].ToString(),
                };
                list.Add(Materias);
            }
            return list;
        }

    }
}