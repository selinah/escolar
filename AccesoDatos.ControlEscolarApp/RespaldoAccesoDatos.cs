﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.ControlEscolarApp
{
    public class RespaldoAccesoDatos
    {
        public bool respaldo(string path,string cosa)
        {
            try
            {
                string destino = cosa;
                Directory.CreateDirectory(destino);
                Ionic.Zip.ZipFile traidor = new Ionic.Zip.ZipFile(Path.Combine(cosa, "ControlEscolar" + string.Format("-{0:yyyy-MM-dd_hh-mm-ss}", DateTime.Now) + ".zip"));
                traidor.AddItem(path);
                traidor.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        
        }
    }
}
