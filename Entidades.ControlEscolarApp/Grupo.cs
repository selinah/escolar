﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Entidades.ControlEscolarApp
{
    public class Grupo
    {
        private int _idGrupo;
        private string _fkmateria;
        private string _carrera;
        private string _nombre;

        public int IdGrupo { get => _idGrupo; set => _idGrupo = value; }
        public string Fkmateria { get => _fkmateria; set => _fkmateria = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}
