﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Alumnos
    {
        private int _ncontrol;
        private string _nombre;
        private string _apellido_pat;
        private string _apellido_mat;
        private string _sexo;
        private string _fecha_nacimiento;
        private string _email;
        private string _telefono;
        private string _estado;
        private string _municipio;
        private string _domicilio;

        public int Ncontrol { get => _ncontrol; set => _ncontrol = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoPaterno { get => _apellido_pat; set => _apellido_pat = value; }
        public string ApellidoMaterno { get => _apellido_mat; set => _apellido_mat = value; }
        public string sexo { get => _sexo; set => _sexo = value; }
        public string FechaNacimiento { get => _fecha_nacimiento; set => _fecha_nacimiento = value; }
        public string Email { get => _email; set => _email = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string estado { get => _estado; set => _estado = value; }
        public string municipio { get => _municipio; set => _municipio = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
    }
}
