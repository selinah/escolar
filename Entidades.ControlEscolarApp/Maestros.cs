﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Maestros
    {
        private string no_control;
        private string nombre;
        private string apellido_paterno;
        private string apellido_materno;
        private string fecha_nacimiento;
        private string estado;
        private string municipio;
        private string sexo;
        private string email;
        private string no_tarjeta;
        private string nombre_doc;

        public string No_control { get => no_control; set => no_control = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido_paterno { get => apellido_paterno; set => apellido_paterno = value; }
        public string Apellido_materno { get => apellido_materno; set => apellido_materno = value; }
        public string Fecha_nacimiento { get => fecha_nacimiento; set => fecha_nacimiento = value; }
        public string Estado { get => estado; set => estado = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string Sexo { get => sexo; set => sexo = value; }
        public string Email { get => email; set => email = value; }
        public string No_tarjeta { get => no_tarjeta; set => no_tarjeta = value; }
        public string Nombre_doc { get => nombre_doc; set => nombre_doc = value; }
    }
}
