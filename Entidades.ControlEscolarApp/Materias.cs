﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Materias
    {
        private int _idMateria;
        private string _nombre;
        private string _materiaantecesor;
        private string _materiapredecesor;
        private string _carrera;

        public int IdMateria { get => _idMateria; set => _idMateria = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Materiaantecesor { get => _materiaantecesor; set => _materiaantecesor = value; }
        public string Materiapredecesor { get => _materiapredecesor; set => _materiapredecesor = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
    }
}
