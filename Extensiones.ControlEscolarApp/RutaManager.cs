﻿using System.IO;
using Entidades.ControlEscolarApp;

namespace Extensiones.ControlEscolarApp
{
    public class RutaManager
    {
        private string _appPath;
        private const string LOGOS = "logos";

        public RutaManager(string appPath)
        {
            _appPath = appPath;
        }
        public string RutaRepositorioLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }
            //Dará en una carpeta llamada lgos
        }

        public void CrearRepositoriosLogos()
        {
            if (!Directory.Exists(RutaRepositorioLogos))
            {
                Directory.CreateDirectory(RutaRepositorioLogos);
            }
        }

        public void CrearRepositorioLogosEscuela(int escuelaId)
        {
            CrearRepositoriosLogos();
            string ruta = Path.Combine(RutaRepositorioLogos, escuelaId.ToString());
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }

        public string RutaLogoEscuela(Escuelas escuela)
        {
            return Path.Combine(RutaRepositorioLogos, escuela.IdEscuela.ToString(), escuela.Logo);
        }
    }
}
