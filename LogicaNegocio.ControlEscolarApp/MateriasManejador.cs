﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using AccesoDatos.ControlEscolarApp;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
    public class MateriasManejador
    {
        private MateriasAccesoDatos _MateriasAccesoDatos;

        public MateriasManejador()
        {
            _MateriasAccesoDatos = new MateriasAccesoDatos();
        }
        public void Eliminar(int idmateria)
        {
            _MateriasAccesoDatos.Eliminar(idmateria);
        }

        public void Guardar(Materias materias)
        {
            _MateriasAccesoDatos.Guardar(materias);
        }
    }
}
