﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AccesoDatos.ControlEscolarApp;
using Entidades.ControlEscolarApp;
using Extension.ControlEscolarApp;

namespace LogicaNegocio.ControlEscolarApp
{
    public class EscuelaManejador
    {
        private EscuelasAccesoDatos _escuelasAccesoDatos = new EscuelasAccesoDatos();
        private RutaManager _rutasManager;

        public EscuelaManejador(RutaManager rutasManager)
        {
            _rutasManager = rutasManager;
        }
        public void guardar (Escuelas escuelas)
        {

        }
        public Escuelas GetEscuelas()
        {
            return _escuelasAccesoDatos.GetEscuelas("");
        }

        public bool CargarLogo(string fileName)
        {
            var archivoNombre = new FileInfo(fileName);
            if (archivoNombre.Length > 5000000)
            {
                return false;
            }
            return true;
        }

        public void LimpiarDoc(int escuelaid, string tipoDocumento)
        {
            string rutaRepositorio = String.Empty;
            string extenion = String.Empty;
            switch (tipoDocumento)
            {
                case "png":
                    rutaRepositorio = _rutasManager.RutaRepositorioLogos;
                    extenion = "*.png";
                    break;
                case "jpg":
                    rutaRepositorio = _rutasManager.RutaRepositorioLogos;
                    extenion = "*.jpg";
                    break;
            }
            string ruta = Path.Combine(rutaRepositorio, escuelaid.ToString());
            if (Directory.Exists(ruta))
            {
                var obtenerArchivos = Directory.GetFiles(ruta, extenion);
                FileInfo archivoAnterior;
                if (obtenerArchivos.Length != 0)
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                    if (archivoAnterior.Exists)
                    {
                        archivoAnterior.Delete();
                    }
                }
            }
        }

        public string GetNombreLogo(string fileName)
        {
            var archivoNombre = new FileInfo(fileName);
            return archivoNombre.Name;
        }

        public void GuardarLogo(string fileName, int escuelaid)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                var archivoDocument = new FileInfo(fileName);
                string ruta = Path.Combine(_rutasManager.RutaRepositorioLogos, escuelaid.ToString());
                if (Directory.Exists(ruta))
                {
                    var obtenerArchivos = Directory.GetFiles(ruta);
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                        }

                    }
                    else
                    {
                        archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                    }
                }
                else
                {
                    _rutasManager.CrearRepositorioLogosEscuela(escuelaid);
                    archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                }
            }
        }
    }
}
