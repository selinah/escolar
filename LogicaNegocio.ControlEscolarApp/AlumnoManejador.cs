﻿using System;
using System.Data;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using AccesoDatos.ControlEscolarApp;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
    public class AlumnoManejador
    {
        private AlumnosAccesoDatos _alumnosAccesoDatos;

        public AlumnoManejador()
        {
            _alumnosAccesoDatos = new AlumnosAccesoDatos();
        }
        public void Eliminar(int id_alumno)
        {
            _alumnosAccesoDatos.Eliminar(id_alumno);
        }

        public void Guardar(Alumnos alumnos)
        {
           // if ((NombreValido (alumnos.Email)== true) && (alumnos.Nombre!="")&&(alumnos.Apellido_pat!="")&&(alumnos.Apellido_pat!=""))
            
                _alumnosAccesoDatos.Guardar(alumnos);
           // }

        }
        public bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$"); //Regex = Regular Expression
            var match = regex.Match(nombre);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public bool TelefonoValido(string nombre)
        {
            var regex = new Regex(@"^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$"); //Regex = Regular Expression
            var match = regex.Match(nombre);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

       // private bool 

        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            list = _alumnosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}
