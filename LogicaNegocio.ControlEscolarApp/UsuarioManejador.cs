﻿using System;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using AccesoDatos.ControlEscolarApp;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _UsuarioAccesoDatos;

        public UsuarioManejador()
        {
            _UsuarioAccesoDatos = new UsuarioAccesoDatos();
        }
        public void Eliminar(int idUsuario)
        {
            _UsuarioAccesoDatos.Eliminar(idUsuario);
        }

        public void Guardar(Usuarios usuarios)
        {
            _UsuarioAccesoDatos.Guardar(usuarios);
        }
        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$"); //Regex = Regular Expression
            var match = regex.Match(nombre);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public List<Usuarios> ObtenerLista(string filtro)
        {
            var list = new List<Usuarios>();
            list = _UsuarioAccesoDatos.ObtenerLista(filtro);
            return list;
        }
     /*   public Tuple<bool, string> EsUsuarioValido (Usuarios usuarioo) //Tuple almacen los campos, como list pero con más tipos de campo
        {
            string mensaje = "";
            bool valido = true;
            if (usuarioo.Nombre.Length == 0)
            {
                mensaje = "El nombre es necesario";
                valido = false;

            }
            else if ((!NombreValido(usuarioo.Nombre))||(!NombreValido(usuarioo.ApellidoPaterno))||(!NombreValido(usuarioo.ApellidoMaterno)))
            {
                mensaje = "El nombre no tiene un formato válido";
                valido = false;
            }
            else if ((usuarioo.Nombre.Length > 15)||(usuarioo.ApellidoPaterno.Length > 15)||(usuarioo.ApellidoMaterno.Length > 15))
            {
                mensaje = "La longitud para nombre";
            }
            else
            {
                valido = true;
            }
            return Tuple.Create(valido, mensaje);

        }*/

    }
}
