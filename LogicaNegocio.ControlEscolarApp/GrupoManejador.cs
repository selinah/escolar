﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using AccesoDatos.ControlEscolarApp;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
    public class GrupoManejador
    {
        private GrupoAccesoDatos _GrupoAccesoDatos;

        public GrupoManejador()
        {
            _GrupoAccesoDatos = new GrupoAccesoDatos();
        }
        public void Eliminar(int idGrupo)
        {
            _GrupoAccesoDatos.Eliminar(idGrupo);
        }

        public void Guardar(Grupo grupo)
        {
            _GrupoAccesoDatos.Guardar(grupo);
        }
    }
}

