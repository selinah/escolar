﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using AccesoDatos.ControlEscolarApp;
using System.Text.RegularExpressions;
using Extension.ControlEscolarApp;

namespace LogicaNegocio.ControlEscolarApp
{
    public class RespaldoManejador
    {
        private RespaldoAccesoDatos _RespaldoAccesoDatos;
        private RutaManager _rutaManager;

        public RespaldoManejador(RutaManager rutaManager)
        {
            _RespaldoAccesoDatos = new RespaldoAccesoDatos();
            _rutaManager = rutaManager;
        }
        public bool respaldo(string rutaparaguardar)
        {
            return _RespaldoAccesoDatos.respaldo(_rutaManager.RutaRepositorioLogos, rutaparaguardar);
        }
            
        }
}
